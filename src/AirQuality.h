#pragma once

#include "Arduino.h"

class AirQualitySensor {
  public:
  AirQualitySensor(int pin);

  bool init(void);
  int slope(void);
  int getValue(void);
  int getQuality(void);

  static const int FORCE_SIGNAL;
  static const int HIGH_POLLUTION;
  static const int LOW_POLLUTION;
  static const int FRESH_AIR;

  protected:
  int _pin;

  int _lastVoltage;
  int _currentVoltage;
  int _standardVoltage;

  long _voltageSum;
  int _volSumCount;
  long _lastStdVolUpdated;

  void updateStandardVoltage(void);
};
