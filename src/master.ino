/* Particle libraries */
#include "Particle.h"

#include <Wire.h>

/* Management libraries */
#include "particle-management/management.h"
#include "variables.h"

/* Sensor Libraries */
#ifdef DISPLAY_ON
#include "display.h"
#endif
#ifdef SENSOR_DHT11
#include "Seeed_DHT11.h"
#endif
#ifdef SENSOR_GL5528
#include "light.h"
#endif
#ifdef SENSOR_HP206C
#include "barometer.h"
#endif
#ifdef SENSOR_LM386
#include "sound.h"
#endif
#ifdef SENSOR_MICS6814
#include "MultiChannelGasSensor.h"
#endif
#ifdef SENSOR_MP503
#include "AirQuality.h"
#endif
#ifdef SENSOR_MQ2
#include "gasSensor.h"
#endif
#ifdef SENSOR_TM2291
#include "motion.h"
#endif

/* Pin, addresses & settings */
#define DISPLAY_CLK D12
#define DISPLAY_DIO D11
#ifndef DISPLAY_BRIGHT
#define DISPLAY_BRIGHT 4
#endif
#define SENSOR_TM2291_PIN D4
#define SENSOR_MP503_PIN A4
#define SENSOR_MQ2_PIN A2
#define SENSOR_GL5528_PIN A5
#define SENSOR_LM386_PIN A0
#define SENSOR_DHT11_PIN A3
#define SENSOR_MICS6814_ADDRESS 0x04
#define SENSOR_HP206C_ADDRESS 0x76

SYSTEM_MODE(SEMI_AUTOMATIC);

/* Device manager */
Management management;

/* Sensors and sensor variables */
#ifdef SENSOR_DHT11
float temperatureAvrg = -1; // previous temperature reading
int temperatureCount = 0; // temperature averages counter
#endif
#ifdef SENSOR_MP503
AirQualitySensor airQualitySensor(SENSOR_MP503_PIN);
#endif
#ifdef SENSOR_MQ2
MQ2 mq2(SENSOR_MQ2_PIN);
#endif
#ifdef SENSOR_DHT11
DHT tempHumSensor(SENSOR_DHT11_PIN);
#endif
#ifdef DISPLAY_ON
Display display(DISPLAY_CLK, DISPLAY_DIO, DISPLAY_BRIGHT);
#endif

void setup()
{
  /* Turn on status LED during setup */
  pinMode(D7, OUTPUT);
  digitalWrite(D7, HIGH);

  /* Prepare sensor pins */
  pinMode(SENSOR_TM2291_PIN, INPUT);
  pinMode(SENSOR_MP503_PIN, INPUT);
  pinMode(SENSOR_MQ2_PIN, INPUT);
  pinMode(SENSOR_GL5528_PIN, INPUT);
  pinMode(SENSOR_LM386_PIN, INPUT);
  pinMode(SENSOR_DHT11_PIN, INPUT);

  /* Prepare sensors */
#ifdef SENSOR_DHT11
  tempHumSensor.begin();
#endif
#ifdef SENSOR_MICS6814
  gas.begin(SENSOR_MICS6814_ADDRESS);
  gas.powerOn();
#endif

  /* Set management settings */
  management.version = VERSION;
  management.branch = BRANCH;
  management.project = PROJECT;
#ifdef PUB_MQTT
  management.useMQTT = true;
  management.MQTTHost = MQTT_HOST;
  management.MQTTPort = MQTT_PORT;
  management.MQTTUser = MQTT_USER;
  management.MQTTPassword = MQTT_PSWD;
#else
  management.useMQTT = false;
#endif
#ifdef PUB_PARTICLE
  management.useParticle = true;
#else
  management.useParticle = false;
#endif
#ifdef PUB_NAME
  management.useParticleName = true;
#endif
#ifdef BLUETOOTH_SCAN_TIMEOUT
  management.bluetoothScanTimeout = BLUETOOTH_SCAN_TIMEOUT;
#endif

  /* Set callbacks */
  management.MQTTCallback = [&](char* t, byte* p, uint l) { management.MQTTControlCallback(t, p, l); };
  management.ParticleCallback = [&](String t, String p) { management.ParticleControlCallback(t, p); };

  /* Connect & subscribe */
  management.connect(SystemMode.mode());
  if (!management.isConnected()) System.reset();
  management.subscribeMQTT("control");

  /* Add streams and publish info */
#ifdef BLUETOOTH_SCAN_TIMEOUT
  management.streams.push_back("bluetooth_devices");
#ifdef BLUETOOTH_SCAN_PUB_MAC
  management.streams.push_back("bluetooth_devices_addresses");
#endif
#endif
#ifdef SENSOR_TM2291
  management.streams.push_back("tm2291_motion");
  management.streams.push_back("tm2291_motion_raw");
#endif
#ifdef SENSOR_MP503
  management.streams.push_back("mp503_pollution");
#endif
#ifdef SENSOR_MQ2
  management.streams.push_back("mq2_lpg");
  management.streams.push_back("mq2_co");
  management.streams.push_back("mq2_smoke");
#endif
#ifdef SENSOR_LM386
  management.streams.push_back("lm386_sound");
#endif
#ifdef SENSOR_GL5528
  management.streams.push_back("gl5528_light");
#endif
#ifdef SENSOR_DHT11
  management.streams.push_back("dht11_temperature");
#endif
#ifdef SENSOR_MICS6814
  management.streams.push_back("mics6814_c2h5oh");
  management.streams.push_back("mics6814_c3h8");
  management.streams.push_back("mics6814_c4h10");
  management.streams.push_back("mics6814_ch4");
  management.streams.push_back("mics6814_co");
  management.streams.push_back("mics6814_h2");
  management.streams.push_back("mics6814_nh3");
  management.streams.push_back("mics6814_no2");
#endif
#ifdef SENSOR_HP206C
  management.streams.push_back("hp206c_temperature");
  management.streams.push_back("hp206c_pressure");
  management.streams.push_back("hp206c_altitude");
#endif

  /* Publish information */
  management.publishInfo("startup");
  management.serialInfo();

  /* Turn off status LED */
  digitalWrite(D7, LOW);

  /* Initialize loopEnd and set the first cycle to last 5 seconds */
  management.loopEnd = Time.now() - (TRANSMISSION_INTERVAL / 1000);
  management.loopEnd += 5;
}

void loop()
{
  management.loopStart();

#ifdef DISPLAY_ON
  /* Write runtime on display */
  display.write(management.runtime() / 1000);
#endif

#ifdef BLUETOOTH_SCAN_TIMEOUT
  Vector<BleScanResult> BLEDevices; // Detected BLE devices
#endif

  /* Every 5 cycles publish runtime and cycles completed */
  if (management.loop % 5 == 1) {
    management.publish("runtime", management.runtime());
    management.publish("cycles", management.loop - 1);
  }

  /* Initialize and get motion, sound and temperature data */
  int intervalCounter = 1; // Number of sub-cycles in transmission interval
#ifdef SENSOR_TM2291
  float motion = getMotion(SENSOR_TM2291_PIN);
#endif
#ifdef SENSOR_LM386
  int sound = getSound(SENSOR_LM386_PIN);
#endif
#ifdef SENSOR_DHT11
  int temperature = tempHumSensor.getTemperature();
  int intervalCounterTemp = 1; // Number of values added to the temperature average
#endif

  while (Time.now() < (management.loopEnd + (TRANSMISSION_INTERVAL / 1000))) {
    management.keepAlive();
    if (!management.isConnected()) System.reset();

    /* Update sensor data */
    intervalCounter++;
#ifdef SENSOR_TM2291
    motion += getMotion(SENSOR_TM2291_PIN);
#endif
#ifdef SENSOR_LM386
    sound += getSound(SENSOR_LM386_PIN);
#endif
#ifdef SENSOR_DHT11
    int temperatureTmp = tempHumSensor.getTemperature();
    if (0 > temperature && temperature < 50) {
      temperature += temperatureTmp;
      intervalCounterTemp++;
    }
#endif

#ifdef DISPLAY_ON
    /* Write runtime on display */
    display.write(management.runtime() / 1000);
#endif

#ifdef BLUETOOTH_SCAN_TIMEOUT
    /* Scan for nearby BLE devices */
    Vector<BleScanResult> BLEDevicesTmp = BLE.scan(); // BLE devices found in scan
    for (auto&& BLEDevice : BLEDevicesTmp) {
      bool add = true;
      for (auto&& BLEDevice2 : BLEDevices) {
        if (BLEDevice.address == BLEDevice2.address) {
          add = false;
          break;
        }
      }
      if (add) BLEDevices.append(BLEDevice);
    }
#else
    delay(100);
#endif
  }

  /* Obtain average of interval-read values */
#ifdef SENSOR_TM2291
  motion /= intervalCounter;
#endif
#ifdef SENSOR_LM386
  sound /= intervalCounter;
#endif
#ifdef SENSOR_DHT11
  temperature /= intervalCounterTemp;
#endif

  /* Get sensor data */
#ifdef SENSOR_MP503
  int pollution = airQualitySensor.getQuality();
#endif
#ifdef SENSOR_MQ2
  int mq2LPG = mq2.getPpmLPG();
  int mq2CO = mq2.getPpmCO();
  int mq2Smoke = mq2.getPpmSmoke();
#endif
#ifdef SENSOR_GL5528
  int light = getLight(SENSOR_GL5528_PIN);
#endif
#ifdef SENSOR_DHT11
  int humidity = tempHumSensor.getHumidity();
#endif
#ifdef SENSOR_MICS6814
  float micsC2H5OH = gas.measure_C2H5OH();
  float micsC3H8 = gas.measure_C3H8();
  float micsC4H10 = gas.measure_C4H10();
  float micsCH4 = gas.measure_CH4();
  float micsCO = gas.measure_CO();
  float micsH2 = gas.measure_H2();
  float micsNH3 = gas.measure_NH3();
  float micsNO2 = gas.measure_NO2();
#endif
#ifdef SENSOR_HP206C
  float baro[3];
  getBarometer(SENSOR_HP206C_ADDRESS, baro);
#endif

  /* Publish sensor data */
#ifdef BLUETOOTH_SCAN_TIMEOUT
  management.publish("bluetooth_devices", BLEDevices.size());
#endif
#ifdef SENSOR_TM2291
  management.publish("tm2291_motion", motion >= 0.1 ? 1 : 0);
  management.publish("tm2291_motion_raw", motion);
#endif
#ifdef SENSOR_MP503
  management.publish("mp503_pollution", pollution);
#endif
#ifdef SENSOR_MQ2
  management.publish("mq2_lpg", mq2LPG);
  management.publish("mq2_co", mq2CO);
  management.publish("mq2_smoke", mq2Smoke);
#endif
#ifdef SENSOR_LM386
  if (sound > 0) management.publish("lm386_sound", sound);
#endif
#ifdef SENSOR_GL5528
  management.publish("gl5528_light", light);
#endif
#ifdef SENSOR_DHT11
  if (humidity > 0 && humidity <= 100) management.publish("dht11_humidity", humidity);
  if (0 > temperature && temperature < 50 && (temperatureAvrg == -1 || abs(temperature - temperatureAvrg) < 10)) {
    temperatureCount++;
    temperatureAvrg = ((temperatureAvrg * (temperatureCount - 1)) + temperature) / temperatureCount;
    management.publish("dht11_temperature", temperature);
  }
#endif
#ifdef SENSOR_MICS6814
  if (micsC2H5OH >= 0) management.publish("mics6814_c2h5oh", micsC2H5OH);
  if (micsC3H8 >= 0) management.publish("mics6814_c3h8", micsC3H8);
  if (micsC4H10 >= 0) management.publish("mics6814_c4h10", micsC4H10);
  if (micsCH4 >= 0) management.publish("mics6814_ch4", micsCH4);
  if (micsCO >= 0) management.publish("mics6814_co", micsCO);
  if (micsH2 >= 0) management.publish("mics6814_h2", micsH2);
  if (micsNH3 >= 0) management.publish("mics6814_nh3", micsNH3);
  if (micsNO2 >= 0) management.publish("mics6814_no2", micsNO2);
#endif
#ifdef SENSOR_HP206C
  if (baro[0] != 0) management.publish("hp206c_temperature", baro[0]);
  if (baro[1] != 0) management.publish("hp206c_pressure", baro[1]);
  if (baro[0] != 0 && baro[1] != 0) management.publish("hp206c_altitude", baro[2]);
#endif

  management.loopClose();
}
