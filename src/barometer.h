#pragma once

#include <Wire.h>
#include <math.h>

void getBarometer(int address, float data[])
{
  int dataRaw[6];

  Wire.begin();
  Wire.beginTransmission(address);
  Wire.write(0x44 | 0x00);
  Wire.endTransmission();

  Wire.beginTransmission(address);
  Wire.write(0x10);
  Wire.endTransmission();

  Wire.requestFrom(address, 6);

  if (Wire.available() == 6) {
    dataRaw[0] = Wire.read();
    dataRaw[1] = Wire.read();
    dataRaw[2] = Wire.read();
    dataRaw[3] = Wire.read();
    dataRaw[4] = Wire.read();
    dataRaw[5] = Wire.read();
  }

  data[0] = (((dataRaw[0] & 0x0F) * 65536) + (dataRaw[1] * 256) + dataRaw[2]) / 100.00;
  data[1] = (((dataRaw[3] & 0x0F) * 65536) + (dataRaw[4] * 256) + dataRaw[5]) / 100.00;

  Wire.beginTransmission(address);
  Wire.write(0x44 | 0x01);
  Wire.endTransmission();

  Wire.beginTransmission(address);
  Wire.write(0x31);
  Wire.endTransmission();

  Wire.requestFrom(address, 3);

  if (Wire.available() == 3) {
    dataRaw[0] = Wire.read();
    dataRaw[1] = Wire.read();
    dataRaw[2] = Wire.read();
  }

  data[2] = ((pow(1013.25 / data[1], 1 / 5.257) - 1) * (data[0] + 273.15)) / 0.0065;
}
