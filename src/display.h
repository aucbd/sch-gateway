#pragma once

#include "Grove_4Digit_Display.h"

#include <stdint.h>

class Display {
  private:
  TM1637* tm1637;

  public:
  Display(int, int, int);
  void clear(void);
  void write(int8_t[4]);
  void write(int);
  void write(float);
};
