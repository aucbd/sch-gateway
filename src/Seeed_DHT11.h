/* Seeed_DHT11 library
 *
 * MIT license
 *
 * */

#pragma once

// how many timing transitions we need to keep track of. 2 * number bits + extra
#define MAXTIMINGS 85

typedef unsigned char u8;
typedef signed char s8;
typedef unsigned short u16;
typedef signed short s16;
typedef unsigned long u32;

class DHT {
  private:
  u8 data[6];
  u8 _pin, _type, _count;
  u32 _lastreadtime;
  bool firstreading;
  int readTemperature();
  int readHumidity();
  bool read();

  public:
  DHT(u8 pin, u8 count = 6);
  void begin();
  int getHumidity();
  int getTemperature();
};
