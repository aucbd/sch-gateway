# sch-gateway

## Cloning

It is important when cloning this repository with git to ensure that the necessary submodules are cloned as well by using the `--recurse-submodules` flag.

```bash
git clone --recurse-submodules git@bitbucket.org:aucbd/sch-gateway.git
```

## Building

To compile this code, run the Python build script located at `particle-build/build.py`. Refer to [`particle-build/README.md`](particle-build/README.md) for details on requirements, usage and build configurations.

The script outputs all information regarding compilation, flashing and saved binaries.

This code supports the following extra settings (to be put in the build configuration file):

* `BLUETOOTH_SCAN_TIMEOUT [milliseconds]` Scan for BLE devices using specified timeout
* `DISPLAY_ON` use display
* `DISPLAY_BRIGHT` Set brightness of display [0, 5]
* `PUB_MQTT` Publish via MQTT
* `PUB_NAME` Get device name from Particle (use serial otherwise)
* `PUB_PARTICLE` Publish to Particle
* `SENSOR_DHT11` Temperature and humidity sensor
* `SENSOR_GL5528` Light sensor (relative)
* `SENSOR_HP206C` Barometer
* `SENSOR_LM386` Sound
* `SENSOR_MICS6814` Multichannel gas sensor (C2H5OH, C3H8, C4H10, CH4, CO, H2, NH3, NO2)
* `SENSOR_MP503` Air quality
* `SENSOR_MQ2` Gas sensor (LPG, CO, smoke)
* `SENSOR_TM2291` Motion
* `TRANSMISSION_INTERVAL [milliseconds]` delay between publish events
* `CONNECTION_TIMEOUT [milliseconds]` timeout for connection attempts (for each stage: WiFi/cellular, MQTT, Particle, etc...)

For a list of all basic settings refer to [`src/particle-management/README.md`](src/particle-management/README.md)

**NOTE**: You will need to populate the `src/variables.h` file manually and add the necessary settings as macros if you plan on compiling without the script.
